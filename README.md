# Google Cloud SQL Backup Utility

This repository contains a command-line utility which can be used to backup
Cloud SQL instances from a Google Cloud Platform project or from all projects
within a Google Cloud Platform folder.

## Preconditions

> See the output from ``gcp-sql-backup --help`` for more information.

The script requires a service account to be configured with appropriate roles
over the project or folder to be backed up and credentials to be obtained. See
the separate [service account setup document](docs/service-account-setup.md) for
more information.

Once credentials have been obtained as a JSON file, set the
``GOOGLE_APPLICATION_CREDENTIALS`` environment variable to point to them.

## Installation

The script can be installed from the repository root via ``pip3 install -e .``
or directly from the git repository via:

```console
$ pip3 install git+https://gitlab.developers.cam.ac.uk/uis/devops/tools/gcp-sql-backup.git
```

## Usage

> See the output from ``gcp-sql-backup --help`` for more information.

Ensure that the ``GOOGLE_APPLICATION_CREDENTIALS`` environment variable is set
appropriately as documented above. To backup project "my-project":

```console
$ GOOGLE_APPLICATION_CREDENTIALS="./credentials.json" \
    gcp-sql-backup --project=my-project --bucket-create-project-default
```

The backups will be created in buckets within the default project of the service
account credentials. The script creates buckets with "coldline" as the default
storage class and with versioning enabled.

## Instance Labels

Labels can be added to the Cloud SQL instances to skip or modify the method of
database export. For both of the following labels, a value of "true" will apply
to all databases in the instance. Alternatively, a comma separated list of
database names can be set that will apply to just those databases (if they exist).

**`skip_backup`** - don't perform a backup

**`export_offload`** - perform the export operation with 'offload' set thus
performing a [serverless export](https://cloud.google.com/sql/docs/postgres/import-export#serverless)

### Examples:

A label of `skip_backup:true` will not backup any databases in the instance.
> This was alternatively achievable by using the command-line argument
> `--instance-filter "NOT settings.userLabels.skip_backup:true"` but is now
> automatically applied.

A label of `export_offload:true` will export all databases in the instance with
'offload' set.

Labels of `skip_backup:db1,db3` and `export_offload:db2,db3` will result in:
- db1 - skipped
- db2 - serverless export
- db3 - skipped (method of export ignored)
- db4 - normal export
