# Service account setup

This document describes a set of ``gcloud`` command which can be used to create
and configure a service account capable of backing up all Cloud SQL instances
within a folder.

Firstly we will specify the name of the service account to create and the
project it should be created in:

```console
# Project and name of service account
$ export PROJECT=some-project-id
$ export SERVICE_ACCT_NAME=backup-client-example
```

Before we proceed, we should check that the required APIs are enabled in the
project. The project containing the service account used by the script should
have the Cloud SQL Admin, Cloud Storage and Resource Manager APIs enabled:

```console
# Enable services
$ gcloud --project=$PROJECT services enable cloudresourcemanager.googleapis.com
$ gcloud --project=$PROJECT services enable sqladmin.googleapis.com
$ gcloud --project=$PROJECT services enable storage-api.googleapis.com
```

The service account can then be created and the generated email address
retrieved:

```console
# Create service account and retrieve email
$ SERVICE_ACCT_EMAIL=$(gcloud --project=$PROJECT iam service-accounts create \
    $SERVICE_ACCT_NAME --display-name="Backup client example"
    --format="value(email)")
```

Specify a file to receive the service account credentials. This is, by
convention, the ``GOOGLE_APPLICATION_CREDENTIALS`` environment variable:

```console
# Where to store credentials
$ export GOOGLE_APPLICATION_CREDENTIALS=$PWD/credentials.json

# Create service account credentials
$ gcloud --project=$Project iam service-accounts keys create \
    $GOOGLE_APPLICATION_CREDENTIALS --iam-account=$SERVICE_ACCT_EMAIL
```

For backing up projects within a folder, the service account should be given
"Folder Viewer" and "Cloud SQL Viewer" roles on the folder.

```console
# Numeric id of folder to grant service account permissions on
$ export FOLDER=123456789

# Grant roles on folder
$ gcloud --project=$PROJECT alpha resource-manager folders \
    add-iam-policy-binding $FOLDER --member=serviceAccount:$SERVICE_ACCT_EMAIL \
    --role=roles/cloudsql.viewer
$ gcloud --project=$PROJECT alpha resource-manager folders \
    add-iam-policy-binding $FOLDER --member=serviceAccount:$SERVICE_ACCT_EMAIL \
    --role=roles/resourcemanager.folderViewer
```

Backups are made to multiple Google Cloud Storage buckets. The service account
should therefore have the "Storage Admin" role in the project where buckets are
to be created.

```console
# Grant roles on project
$ gcloud --project=$PROJECT projects add-iam-policy-binding $PROJECT \
    --member=serviceAccount:$SERVICE_ACCT_EMAIL --role=roles/storage.admin
```
