"""
Backup Google Cloud Platform project Cloud SQL instances to a bucket

Usage:
    gcp-sql-backup (-h | --help)
    gcp-sql-backup [--project=ID...] [--folder=ID...] [--exclude-folder=ID...]
                   (--bucket-create-project-default | --bucket-create-project=ID)
                   [--bucket-create-location=LOCATION] [--quiet] [--parallel=N]
                   [--lifecycle-delete-rule-age=N] [--bucket-patch-lifecycle-policy]
                   [--bucket-patch-retention-policy] [--retention-period=N] [--enable-bucket-lock]
                   [--max-retry-attempts=N ] [--tag=TAG] [--to=PATTERN] [--dry-run] [--recursive]
                   [--exclude-project-pattern=PATTERN...] [--instance-filter=EXPRESSION]

Options:
    -h, --help          Show a brief usage summary.

    --project=ID        Backup all Cloud SQL instances in specified project.

    --folder=ID         Backup all Cloud SQL instances for for projects contained in specified
                        folder. Sub-folders are ignored unless --recursive is specified.

    --recursive         Recursively examine sub-folders of any folders specified.

    --exclude-folder=ID Specify a folder to exclude from project search. Useful when combined with
                        --recursive.

    --exclude-project-pattern=PATTERN
                        A set of shell-like glob patterns which specify project ids which should
                        *not* be backed up.

    --instance-filter=EXPRESSION
                        A filter expression that filters SQL instances to be backed up. See the
                        filter parameter of the sql instances.list method:
                        https://cloud.google.com/sql/docs/postgres/admin-api/rest/v1beta4/instances/list

    --bucket-create-project-default
                        Use the default Application Credentials' project to create buckets.

    --bucket-create-project=ID
                        Specify project id of project to create backup buckets in.

    --bucket-create-location=LOCATION
                        Location to create buckets in. See
                        https://cloud.google.com/storage/docs/locations [default: europe-west2]

    --lifecycle-delete-rule-age=N
                        Set the age at which backup objects will be deleted via a configured
                        lifecycle rule. Default value is 180 days. Will be used in storage buckets'
                        lifecycle policy config. If bucket already exists and lifecycle policy
                        change is needed, this option must be used
                        with --bucket-patch-lifecycle-policy flag. [default: 180]

    --bucket-patch-lifecycle-policy
                        Use this flag if existing buckets' lifecycle policy patching is needed

    --retention-period=N
                        Set retention period for backups. Default value is 91 days.
                        Will be used in storage buckets' retention policy config.
                        If bucket already exists and retention change is needed, this option
                        must be used with --bucket-patch-retention-policy flag. [default: 91]

    --bucket-patch-retention-policy
                        Use this flag if existing buckets' retention policy patching is needed.

    --enable-bucket-lock
                        Use this flag to lock the retention period for the buckets. This is an
                        irreversible action!!!,
                        see https://cloud.google.com/storage/docs/bucket-lock.

    --max-retry-attempts=N
                        How many times the script should retry the backup operation in case
                        of failure [default: 1]

    --to=PATTERN        Storage URL pattern specifying destination bucket and path for backups.
                        [default: gs://{tag}-{project}-{instance}/{database}/{timestamp}.sql.gz]

    --tag=TAG           Tag which can be used to make bucket names unique. [default: backup-sql]

    --parallel=N        Perform operations with N-way parallelism over instances. [default: 1]

    --dry-run           Log operations which would be performed but do not perform them.

Introduction:

    The gcp-sql-backup utility can be used to automate the backup of Google Cloud SQL instances to
    Google Cloud Storage buckets. It will automatically backup all instances found within given
    projects or within all projects within given folders. Multiple projects and folders can be
    specified and all projects which match any of the specified project ids or folder ids will be
    backed up. Note: if given a folder, the utility does not recurse into sub-folders to find
    projects unless the --recursive flag is specified.

    Google application default credentials are used to find projects, initiate export and create or
    modify storage buckets. It is recommended that a service account be used with this script.
    Setting the GOOGLE_APPLICATION_CREDENTIALS environment variable to the location of the service
    account JSON credentials will result in those credentials being used by this script.

    The bucket pattern (see below) is used to construct a storage URL for creating database
    backups.  If the bucket does not exist, it is created in the specified location with the
    "coldline" storage class and with versioning enabled.

    By default, newly created buckets will be configured with a lifecycle policy to delete files
    older than 180 days. This can be changed by using --lifecycle-delete-rule-age=N option. If
    existing buckets need to be patched the --bucket-patch-lifecycle-policy option can be used.
    Newly created buckets will also be configured with a retention period of 91 days. This can be
    changed using --retention-period=N option and can be locked using the --enable-bucket-lock
    flag. If existing buckets need to be patched the --bucket-patch-retention-policy option can be
    used.

Required APIs:

    The project containing the service account used to run this script should have the Resource
    Manager, Cloud Storage and Cloud SQL Admin APIs enabled.

Permissions and Roles:

    When backing up all instances within a folder it is recommended that the account corresponding
    to the default credentials be given the "Folder Viewer" and "Cloud SQL Viewer" roles on the
    folder. This will ensure that appropriate roles are inherited by the projects within. For
    backing up a single project, it is sufficient to simply grant "Project Viewer" and "Cloud SQL
    Viewer" roles on the project.

    The service account being used by this script should have the "Storage Admin" role on the
    target buckets. Since this script may create buckets it is recommended that the storage account
    be given the "Storage Account" role in the bucket project.

    To export to a bucket, a Cloud SQL instance requires the "Storage Admin" role on a bucket. This
    script will automatically grant that role to the instance if it is not already granted. Since
    this permission is granted bucket-wide, it is recommended that the storage bucket pattern be
    such that a separate storage bucket is created per instance. That way Cloud SQL instances will
    not have the ability to overwrite backups from other instances.

Bucket Pattern:

    The destination of the backup is specified using a storage URL pattern. This is a Python format
    string with the following variables available:

    * project: the project id of the project containing the Cloud SQL instance
    * instance: the name of the Cloud SQL instance
    * database: the name of the database within the Cloud SQL instance
    * timestamp: a timestamp of the form YYYYMMDD-HHMMSS
    * tag: an arbitrary tag (see the "--tag" flag)
    * create_project: the project id of the project which buckets are created in

Parallelism:

    This script can parallelise operations over instances. A given instance can only backup one
    database at a time but across instances backups can be performed in parallel. By default this
    script uses 1-way parallelism (i.e. it performs backups sequentially). N-way parallelism can be
    specified on the command line which will decrease the runtime of the script but make logs
    harder to interpret. Note that making "N" too large will likely result in the script's service
    account hitting other rate limits on simultaneous access to resources.

"""

import datetime
import fnmatch
import logging
import multiprocessing
import os
import socket
import sys
import time
from urllib.parse import urlparse

import docopt
import google.auth
from google.auth import impersonated_credentials
from google.cloud import exceptions as google_exc
from google.cloud import resourcemanager_v3, storage
from googleapiclient.discovery import build

# Create a logger for the script.
LOG = logging.getLogger(os.path.basename(sys.argv[0]))

# Permissions required on a bucket for a Cloud SQL instance to be able to export a dump of itself
# to the bucket.
SQL_INSTANCE_BUCKET_ROLES = [
    "roles/storage.objectAdmin",
]

# Maximum number of seconds to wait for an operation to finish.
MAX_OPERATION_WAIT = 45 * 60  # 45 minutes

# Poll delay while waiting for operations in seconds.
OPERATION_POLL_DELAY = 5

# How long to wait after a failure to prevent next operation being blocked by failing one
POST_FAILURE_DELAY = 60

# Shared lock for performing bucket operations. This is necessary when using n-way parallelism over
# SQL instances since we don't want to have two independent processes trying to create the same
# bucket.
BUCKET_LOCK = multiprocessing.Lock()

# List of MySQL system database names which must be excluded from the mysqldump export.
MYSQL_SYSTEM_DATABASES = ["information_schema", "mysql", "performance_schema", "sys"]

# Set the socket timeout to a higher default to try to resolve occasional failures due to socket
# timeout errors:
# https://github.com/googleapis/google-api-python-client/issues/632#issuecomment-541973021
socket.setdefaulttimeout(180)  # 3 minutes


def main():
    # Parse command line options
    opts = docopt.docopt(__doc__)

    # Configure logging verbosity
    logging.basicConfig(level=logging.WARN if opts["--quiet"] else logging.INFO)

    # HACK: make the googleapiclient.discovery module less spammy in the logs
    logging.getLogger("googleapiclient.discovery").setLevel(logging.WARN)
    logging.getLogger("googleapiclient.discovery_cache").setLevel(logging.ERROR)

    # Get the default Google credentials. Store the default project in case we are to use it for
    # bucket creation.
    default_credentials, default_credentials_project = google.auth.default()
    LOG.info(
        'Default credentials are configured with the default project id "%s"',
        default_credentials_project,
    )

    if os.getenv("GOOGLE_IMPERSONATE_SERVICE_ACCOUNT"):
        credentials = impersonated_credentials.Credentials(
            source_credentials=default_credentials,
            target_principal=os.getenv("GOOGLE_IMPERSONATE_SERVICE_ACCOUNT"),
            target_scopes=["https://www.googleapis.com/auth/cloud-platform"],
            lifetime=500,
        )
    else:
        credentials = default_credentials

    # Log the credentials being used. Depending on the type of credentials, some of these
    # attributes will not be present.
    service_account_email = getattr(credentials, "service_account_email", None)
    if service_account_email is not None:
        LOG.info('Using credentials for service account "%s".', service_account_email)

    client_id = getattr(credentials, "client_id", None)
    if client_id is not None:
        LOG.info('Using credentials for client id "%s".', client_id)

    # If we couldn't determine the form of credentials being used, log the Python repr() output as
    # a last resort.
    if client_id is None and service_account_email is None:
        LOG.warn('Using unknown form of credentials: "%r".', credentials)

    # Determine which project should be used for bucket creation.
    if opts["--bucket-create-project-default"]:
        create_project = default_credentials_project
    else:
        create_project = opts["--bucket-create-project"]
    LOG.info('Creating required buckets in project "%s"', create_project)

    # Get list of projects to process and warn if this list if empty.
    projects = get_projects(opts, credentials=credentials)
    if len(projects) == 0:
        LOG.warn("No projects matched the specification.")

    # Form a list of project object, instance resource pairs which should be backed up
    LOG.info("Fetching SQL instances to backup...")
    backup_targets = []
    instance_filter = opts["--instance-filter"] if opts["--instance-filter"] else ""
    for project in projects:
        sql_service = build("sqladmin", "v1beta4", credentials=credentials)
        instances = list_all(
            sql_service.instances().list,
            project=project.project_id,
            filter=instance_filter,
        )

        if len(instances) == 0:
            LOG.info('Project "%s" has no Cloud SQL instances', project.project_id)

        backup_targets.extend((project, instance) for instance in instances)
    LOG.info(
        "In total there are %s Cloud SQL instance(s) to backup.", len(backup_targets)
    )

    # Parallelise export operations over instances.
    LOG.info("Using %s-way parallelism over instances.", int(opts["--parallel"]))
    with multiprocessing.Pool(int(opts["--parallel"])) as pool:
        # An error callback to log failure if there is an error backing up an instance.
        def error_callback(e):
            LOG.error('Error in backup process: "%s".', e, exc_info=e)

        # Launch async tasks and gather list of AsyncResult objects.
        results = [
            pool.apply_async(
                backup_cloud_sql_instance,
                kwds={
                    "instance": instance,
                    "project_id": project.project_id,
                    "bucket_pattern": opts["--to"],
                    "create_project": create_project,
                    "create_location": opts["--bucket-create-location"],
                    "tag": opts["--tag"],
                    "dry_run": opts["--dry-run"],
                    "lifecycle_delete_rule_age": opts["--lifecycle-delete-rule-age"],
                    "patch_lifecycle_policy": opts["--bucket-patch-lifecycle-policy"],
                    "retention_period": opts["--retention-period"],
                    "patch_retention_policy": opts["--bucket-patch-retention-policy"],
                    "enable_bucket_lock": opts["--enable-bucket-lock"],
                    "max_retry_attempts": int(opts["--max-retry-attempts"]),
                },
                error_callback=error_callback,
            )
            for project, instance in backup_targets
        ]

        # Wait for all tasks to complete.
        for r in results:
            r.wait()

        # Check that all tasks succeeded.
        if not all(r.successful() for r in results):
            LOG.error("At least one backup process failed")
            sys.exit(1)

    LOG.info("Backup completed successfully")


def check_operation_for_errors(operation, database, instance):
    # Check for and log any errors in operation.
    error = operation.get("error")
    if error is not None:
        for e in error.get("errors", []):
            LOG.error(
                'Error requesting export of database "%s" on instance "%s" (%s): "%s".',
                database["name"],
                instance["name"],
                e.get("code", "No code"),
                e.get("message", "No message"),
            )
        return True
    return False


def backup_cloud_sql_instance(
    *,
    instance,
    project_id,
    bucket_pattern,
    create_project,
    create_location,
    tag,
    dry_run,
    lifecycle_delete_rule_age,
    patch_lifecycle_policy,
    retention_period,
    patch_retention_policy,
    enable_bucket_lock,
    max_retry_attempts,
):
    """
    Given an instance resource dict and a project id, backup any databases within the instance.

    """
    LOG.info('Backing up instance "%s"...', instance["name"])

    labels = instance.get("settings", {}).get("userLabels", {})

    # Bucket client expects the retention period in seconds but we specify the arg in days.
    retention_period_secs = int(retention_period) * 86400

    # `skip_backup` label used to skip backing up entire instance or specific databases
    skip_backup = labels.get("skip_backup", "").lower().strip()
    if skip_backup == "true":
        LOG.info("SKIPPING: skip_backup:true label on instance")
        return
    skip_databases = [d.strip() for d in skip_backup.split(",")]

    # `export_offload` label used to flag that all databases in this instance or specific ones
    # should be exported with `offload` set
    export_offload = labels.get("export_offload", "").lower().strip()
    offload_databases = [d.strip() for d in export_offload.split(",")]
    if export_offload == "true":
        LOG.info("Exporting with 'offload' for all databases in instance")

    # Re-fetch the default credentials since this function may be called within a subprocess and we
    # don't want to have multiple processes trying to use the same refresh token.
    default_credentials, _ = google.auth.default()
    if os.getenv("GOOGLE_IMPERSONATE_SERVICE_ACCOUNT"):
        credentials = impersonated_credentials.Credentials(
            source_credentials=default_credentials,
            target_principal=os.getenv("GOOGLE_IMPERSONATE_SERVICE_ACCOUNT"),
            target_scopes=["https://www.googleapis.com/auth/cloud-platform"],
            lifetime=500,
        )
    else:
        credentials = default_credentials

    # Create API clients
    sql_service = build("sqladmin", "v1beta4", credentials=credentials)
    storage_client = storage.Client(credentials=credentials, project=create_project)

    # Find out the email address for the service account associated with the Cloud SQL instance.
    instance_service_account = instance.get("serviceAccountEmailAddress", "")
    if instance_service_account == "":
        raise RuntimeError(
            f'Instance "{instance["name"]} does not have an associated service account'
        )

    # Get list of databases to export.
    # Note: databases list is not paged so we do not need to use list_all().
    databases = (
        sql_service.databases()
        .list(project=project_id, instance=instance["name"])
        .execute()
        .get("items", [])
    )

    # Cloud SQL exports for MySQL databases must not include the system databases.
    if instance["databaseVersion"].lower().startswith("mysql"):
        databases = [d for d in databases if d["name"] not in MYSQL_SYSTEM_DATABASES]

    if len(databases) == 0:
        LOG.info('Instance "%s" has no databases.', instance["name"])

    # Export each database in turn.
    for database in databases:
        db_name = database["name"]

        LOG.info(
            'Backing up database "%s" from instance "%s"...', db_name, instance["name"]
        )

        if db_name in skip_databases:
            LOG.info("SKIPPING: database in 'skip_backup' label")
            continue
        offload = export_offload == "true" or db_name in offload_databases
        if offload:
            LOG.info("Exporting with 'offload'")

        # Form storage URL for backup
        timestamp = datetime.datetime.utcnow().strftime("%Y%m%d-%H%M%S")
        bucket_url = bucket_pattern.format(
            instance=instance["name"],
            database=db_name,
            project=project_id,
            timestamp=timestamp,
            tag=tag,
            create_project=create_project,
        )

        LOG.info('Backing up to "%s".', bucket_url)

        bucket, path = parse_storage_url(bucket_url)

        if dry_run:
            LOG.info("SKIPPING: Dry run")
            continue

        # Ensure that the bucket exists and that Cloud SQL instance's service account has correct
        # roles on the bucket. Do this with the bucket lock held since we're creating buckets
        # and/or patching IAM policy and we don't want any other process to be doing these things
        # at the same time.
        with BUCKET_LOCK:
            try:
                bucket_client = storage_client.get_bucket(bucket)
                if patch_lifecycle_policy:
                    bucket_client.clear_lifecycle_rules()
                    bucket_client.add_lifecycle_delete_rule(
                        age=lifecycle_delete_rule_age
                    )
                    bucket_client.patch()

                if patch_retention_policy and not bucket_client.retention_policy_locked:
                    # Versioning cannot be enabled when using a retention policy.
                    bucket_client.versioning_enabled = False
                    bucket_client.retention_period = retention_period_secs
                    bucket_client.patch()

                    if enable_bucket_lock:
                        # Locking a retention policy is an irreversible action!
                        bucket_client.lock_retention_policy()
            except google_exc.NotFound:
                LOG.info(
                    'Creating bucket "%s" in project "%s", location "%s"',
                    bucket,
                    create_project,
                    create_location,
                )
                storage.Bucket(storage_client, name=bucket).create(
                    project=create_project, location=create_location
                )

                # We re-fetch the bucket to make sure that it was successfully created.
                bucket_client = storage_client.get_bucket(bucket)
                bucket_client.storage_class = "COLDLINE"

                # Versioning cannot be enabled when using a retention period.
                bucket_client.versioning_enabled = False
                bucket_client.add_lifecycle_delete_rule(age=lifecycle_delete_rule_age)
                bucket_client.retention_period = retention_period_secs
                bucket_client.patch()

                if enable_bucket_lock:
                    # Locking a retention policy is an irreversible action!
                    bucket_client.lock_retention_policy()

                # Re-fetch again to make sure the changes were applied. We don't make these
                # checks for existing buckets because they may have been created by other means.
                bucket_client = storage_client.get_bucket(bucket)
                if bucket_client.storage_class != "COLDLINE":
                    raise RuntimeError(
                        f"Bucket has unexpected storage class: {bucket_client.storage_class}"
                    )

            # Patch the IAM policy if necessary to grant the instance's service account the
            # required roles on the bucket.
            iam_policy = bucket_client.get_iam_policy()
            instance_service_account_member = iam_policy.service_account(
                instance_service_account
            )
            for role in SQL_INSTANCE_BUCKET_ROLES:
                role_members = iam_policy.get(role, {})
                if instance_service_account_member not in role_members:
                    LOG.info(
                        'Granting role "%s" for instance "%s" service account "%s".',
                        role,
                        instance["name"],
                        instance_service_account,
                    )
                    role_members.add(instance_service_account_member)
                    iam_policy[role] = role_members
                    iam_policy = bucket_client.set_iam_policy(iam_policy)
                else:
                    LOG.info(
                        'Instance "%s" service account "%s" already has role "%s" on bucket.',
                        instance["name"],
                        instance_service_account,
                        role,
                    )

        # Start the export operation.
        retry_count = 0
        while retry_count <= max_retry_attempts:
            operation = (
                sql_service.instances()
                .export(
                    project=project_id,
                    instance=instance["name"],
                    body={
                        "exportContext": {
                            "kind": "sql#exportContext",
                            "fileType": "SQL",
                            "uri": bucket_url,
                            "databases": [db_name],
                            "offload": offload,
                        },
                    },
                )
                .execute()
            )
            if not check_operation_for_errors(operation, database, instance):
                break
            retry_count += 1
            LOG.info(
                "Retrying... Attempt %s out of %s", retry_count, max_retry_attempts
            )
        if check_operation_for_errors(operation, database, instance):
            raise RuntimeError("Error issuing export operations")

        # Record when we start operation
        start_time = datetime.datetime.now()

        # Wait for the operation to complete.
        LOG.info(
            'Waiting for operation "%s" on instance "%s" to finish...',
            operation["name"],
            instance["name"],
        )
        # Double maximum wait for offload exports (as they take longer)
        max_wait = MAX_OPERATION_WAIT * (2 if offload else 1)
        for _ in range(max(1, max_wait // OPERATION_POLL_DELAY)):
            current_op = (
                sql_service.operations()
                .get(project=project_id, operation=operation["name"])
                .execute()
            )

            # Get a human-friendly duration (minutes and seconds)
            duration = (datetime.datetime.now() - start_time).seconds
            duration_human = (
                f"{duration // 60}:{duration % 60 :>02}"  # noqa: E231, E203
            )

            LOG.info(
                'Operation "%s" on instance "%s" (%s) status is: "%s". [%s]',
                operation["name"],
                instance["name"],
                db_name,
                current_op["status"],
                duration_human,
            )
            if current_op["status"] == "DONE":
                if check_operation_for_errors(current_op, database, instance):
                    # Wait for this operation to truly finish (a CANCELLED operation can prevent
                    # the next operation completing)
                    time.sleep(POST_FAILURE_DELAY)
                break
            time.sleep(OPERATION_POLL_DELAY)
        else:
            raise RuntimeError("Timed out waiting for operation")


def list_all(list_cb, *, items_key="items", **kwargs):
    """
    Simple wrapper for Google Client SDK list()-style callables. Repeatedly fetches pages of
    results merging all the responses together. Returns the merged "items" arrays from the
    responses. The key used to get the "items" array from the response may be overridden via the
    items_key argument.

    """
    # Loop while we wait for nextPageToken to be "none"
    page_token = None
    resources = []
    while True:
        list_response = list_cb(pageToken=page_token, **kwargs).execute()
        resources.extend(list_response.get(items_key, []))

        # Get the token for the next page
        page_token = list_response.get("nextPageToken")
        if page_token is None:
            break

    return resources


def parse_storage_url(url):
    """
    Parse a gs://bucket/path style storage URL into bucket and path components. If the URL is in
    some way invalid, ValueError is raised.

    Returns a bucket, path pair.

    >>> parse_storage_url('gs://foo/')
    ('foo', '/')
    >>> parse_storage_url('gs://foo/bar/buzz')
    ('foo', '/bar/buzz')
    >>> parse_storage_url('s3://foo/')
    Traceback (most recent call last):
        ...
    ValueError: Storage URL must have "gs" scheme.
    >>> parse_storage_url('gs://foo/?a=1')
    Traceback (most recent call last):
        ...
    ValueError: Storage URL must have empty query.
    >>> parse_storage_url('gs://foo/#a')
    Traceback (most recent call last):
        ...
    ValueError: Storage URL must have empty fragment.
    >>> parse_storage_url('gs://foo:123/')
    Traceback (most recent call last):
        ...
    ValueError: Storage URL must have empty port.

    """
    url_parts = urlparse(url)

    if url_parts.scheme != "gs":
        raise ValueError('Storage URL must have "gs" scheme.')

    if url_parts.query != "":
        raise ValueError("Storage URL must have empty query.")

    if url_parts.fragment != "":
        raise ValueError("Storage URL must have empty fragment.")

    if url_parts.port is not None:
        raise ValueError("Storage URL must have empty port.")

    return url_parts.netloc, url_parts.path


def get_projects(opts, *, credentials):
    """
    Given the command-line flags, return a list of google.cloud.resourcemanager_v3.types.Project
    instances for all the matched projects. Only active projects are returned. (I.e. projects
    which are deleted or marked for deletion are not returned.)

    """
    projects_client = resourcemanager_v3.ProjectsClient(credentials=credentials)
    folders_client = resourcemanager_v3.FoldersClient(credentials=credentials)

    LOG.info("Specified projects: %s", ", ".join(opts["--project"]))

    # Retrieve all projects specified and which are active. Store projects in a dict keyed by
    # project id so we de-duplicate, especially when listing projects in folders below.
    all_projects_by_id = {}
    for n in opts["--project"]:
        p = projects_client.get_project(name=f"projects/{n}")
        if p.state == resourcemanager_v3.Project.State.ACTIVE:
            all_projects_by_id[p.project_id] = p

    # Form a set of folder ids to scan.
    excluded_folders = set(opts["--exclude-folder"])
    folders = set(opts["--folder"]) - excluded_folders

    # If --recursive is specified, extend folder list with sub-folders.
    if opts["--recursive"]:
        # Classic recursive search with memory from CS 101 :).
        folders_to_search = set(folders)
        while len(folders_to_search) > 0:
            # Get next folder in "to search" list.
            parent_folder = folders_to_search.pop()

            # Early out if we're excluding this folder.
            if parent_folder in excluded_folders:
                continue

            # Add the parent to the list of explored folders.
            folders.add(parent_folder)
            LOG.info("Listing sub-folders of %s", parent_folder)

            # Only add folders which have not already been searched to folders_to_search.
            folders_to_search |= (
                set(
                    f.name.split("/")[1]  # "name" is of the form folders/<id>
                    for f in folders_client.list_folders(
                        parent=f"folders/{parent_folder}"
                    )
                )
                - folders
            )

    LOG.info("Specified folders: %s", ", ".join(folders))

    # List all projects in the specified folders, adding the active ones to our dict
    for folder in folders:
        all_projects_by_id.update(
            {
                p.project_id: p
                for p in projects_client.list_projects(parent=f"folders/{folder}")
                if p.state == resourcemanager_v3.Project.State.ACTIVE
            }
        )

    # Form a set of all project ids
    all_project_ids_set = set(all_projects_by_id.keys())

    # Remove any project ids matching specified globs.
    for pattern in opts["--exclude-project-pattern"]:
        all_project_ids_set -= {
            id_ for id_ in all_project_ids_set if fnmatch.fnmatch(id_, pattern)
        }

    # Sort project ids for ease of reading.
    all_project_ids = sorted(all_project_ids_set)
    LOG.info("Projects to back up: %s", ", ".join(all_project_ids))

    # Return projects in the same order as all_project_ids
    return [all_projects_by_id[id_] for id_ in all_project_ids]
