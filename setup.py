import os

from setuptools import find_packages, setup


def load_requirements():
    """
    Load requirements file and return non-empty, non-comment lines with leading and trailing
    whitespace stripped.
    """
    with open(os.path.join(os.path.dirname(__file__), "requirements.txt")) as f:
        return [
            line.strip()
            for line in f
            if line.strip() != "" and not line.strip().startswith("#")
        ]


setup(
    name="gcp-sql-backup",
    version="0.9.3",
    packages=find_packages(),
    install_requires=load_requirements(),
    entry_points={
        "console_scripts": [
            "gcp-sql-backup=gcpsqlbackup:main",
        ]
    },
)
